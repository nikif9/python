from docxtpl import DocxTemplate
import docx
import re
import os
class DocClass:
    path = ''
    def setPathToFile(self, path): #ставка пути к файлу и запомнание его
        self.path = path
        self.name = os.path.splitext(self.path)
    def getPathToFile(self): #получение пути к файлу
        return self.path
    def getFileName(self): #получение имени файла
        return self.name[0]
    def getFileExtendet(self): #получение расширения файла
        return self.name[1]
    def readFileAndFindMask(self): #получение всех масок в документе
        doc = docx.Document(self.path)
        text = ''
        for paragraph in doc.paragraphs:
            text += paragraph.text+ '\n'
        return re.findall(r'\{\{\s*\w+\s*\}\}', text)
    def saveFile(self, context):#сохранение нового документа
        doc = DocxTemplate(self.path)
        doc.render(context)
        doc.save(self.name[0] + "-final.docx")
