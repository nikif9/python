import sqlite3
import os
class Db:
    def __init__(self):
        self.path = os.path.abspath('mydatabase.db')
        self.conn = sqlite3.connect(self.path) 
        self.cursor = self.conn.cursor()
        self.createTable()
    def createTable(self): #создаем таблицу если её нет
        self.cursor.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='login'")
        if self.cursor.fetchone()[0] == 0:
            self.cursor.execute("CREATE TABLE `login` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `login` varchar(30) NOT NULL, `password` varchar(32) NOT NULL)")
            self.conn.commit()
    def findInlogin(self, login): #поиск в базе данных по логину
        self.cursor.execute('SELECT * FROM login WHERE login=?', (login,))
        return self.cursor.fetchone()
    def setInTable(self, login, password): # добавление информации о пользователе в базу данных 
        self.cursor.execute("insert into login (login, password) values (?, ?)", (login, password))
        self.conn.commit()