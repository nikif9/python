from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
import os
import re
import pymorphy2
from tkcalendar import DateEntry
import datetime
from db import Db
from doc import DocClass
from babel.numbers import decimal, format_decimal
class Draw:
    entry = {}
    label = {}
    mask = []
    def __init__(self):
        self.db = Db()

        self.doc = DocClass()

        self.morph = pymorphy2.MorphAnalyzer()

        self.window = Tk()
        self.window.title("Welcome to LikeGeeks app")
        self.window.geometry('700x300')

        self.loginLabel = Label(self.window, text="login: ")
        self.loginLabel.place(relx = 0.5, rely = 0.4, anchor = 's')

        self.loginEnter = Entry(self.window, width=30)
        self.loginEnter.place(relx = 0.5, rely = 0.45, anchor = 's')

        self.passwordLabel = Label(self.window, text="password")
        self.passwordLabel.place(relx = 0.5, rely = 0.5, anchor = 's')

        self.passwordEnter = Entry(self.window, width=30)
        self.passwordEnter.place(relx = 0.5, rely = 0.55, anchor = 's')

        self.authBtn = Button(self.window, text="Авторизация", command=self.login)
        self.authBtn.place(relx = 0.4, rely = 1, anchor = 's') 

        self.regisBtn = Button(self.window, text="регистрация", command=self.registration)
        self.regisBtn.place(relx = 0.6, rely = 1, anchor = 's') 

        self.window.mainloop()
    def browse(self): # функция открытия файла
        self.doc.setPathToFile(filedialog.askopenfilename(title = "Select file",filetypes = (("Word Document","*.docx *.doc"),("all files","*.*")))) #запоминаем путь файла
        self.pathToFile.configure(text = self.doc.getPathToFile()) #  пишем путь файла для того чтобы человек понимал какой сейчас файл открыт
        self.clearGraphic() # удаляем прошлую графику для того чтобы страрые инпут не мешали новым
        if os.path.exists(self.doc.getPathToFile()) and os.path.isfile(self.doc.getPathToFile()):
            if self.doc.getFileExtendet() in ('.docx', '.doc'):
                self.drawInput()
            
    def save(self): # функция сохранение файла
        if self.doc.getPathToFile() == '':
            messagebox.showerror("Ошибка", "выберите word документ")
            return False
        context = self.writeContext() # подготовка словоря для сопостолвение для ворд документа
        if context == False:
            return False
        self.doc.saveFile(context)
        messagebox.showinfo("успешно", "файл создан в директории: " + self.doc.getFileName() + "-final.pdf")
    def login(self): # функция входа
        login = self.loginEnter.get().strip()
        password = self.passwordEnter.get().strip()
        if login == "" or password == "":
            messagebox.showerror("Ошибка", "заполните все поля")
        else:
            data = self.db.findInlogin(login)# проверка есть ли такой логин в базе днных если нет то пишем что не нашли а если нашли то сравниваем пароли
            # print()
            if data != None:           
                if data[2] == password:
                    messagebox.showinfo("успешно", "вы успешно авторизировались")

                    self.drawCoreWindow() # отрисовка окна
                else:
                    messagebox.showerror("Ошибка", "неверный пароль")
            else:
                messagebox.showerror("Ошибка", "такой Логин не найден")
    def registration(self): # функция регестрации
        login = self.loginEnter.get().strip()
        password = self.passwordEnter.get().strip()
        if login == "" or password == "":
            messagebox.showerror("Ошибка", "заполните все поля")
        else:
            data = self.db.findInlogin(login)
            if data == None:           
                self.db.setInTable(login, password)
                messagebox.showinfo("успешно", "аккаунт успешно создан и залогинен")
                self.drawCoreWindow()
            else:
                messagebox.showerror("Ошибка", "уже существует такой логин")
    def drawNameInput(self, par, row): # отрисовка инпутов для имен
        self.label[par] = {}
        self.entry[par] = {}

        self.entry[par]['gender'] = BooleanVar()
        self.entry[par]['gender'].set(0)
        self.label[par]['label'] = Label(self.window, text=par)
        self.label[par]['label'].place(x = 1, y = row, anchor = 'nw') 

        self.label[par]['labelSec'] = Label(self.window, text='фамилия')
        self.label[par]['labelSec'].place(x = 100, y = row, anchor = 'nw') 

        self.entry[par]['entrySec'] = Entry(self.window, width=15)
        self.entry[par]['entrySec'].place(x = 160, y = row, anchor = 'nw') 

        self.label[par]['labelName'] = Label(self.window, text='Имя')
        self.label[par]['labelName'].place(x = 255, y = row, anchor = 'nw') 

        self.entry[par]['entryName'] = Entry(self.window, width=15)
        self.entry[par]['entryName'].place(x = 290, y = row, anchor = 'nw') 

        self.label[par]['labelLast'] = Label(self.window, text='Отчество')
        self.label[par]['labelLast'].place(x = 385, y = row, anchor = 'nw') 

        self.entry[par]['entryLast'] = Entry(self.window, width=15)
        self.entry[par]['entryLast'].place(x = 445, y = row, anchor = 'nw') 

        self.entry[par]['radioMele'] = Radiobutton(text="мужчина", value=0, variable=self.entry[par]['gender'])
        self.entry[par]['radioMele'].place(x = 530, y = row, anchor = 'nw') 
        
        self.entry[par]['radioFemele'] = Radiobutton(text="Женщина", value=1, variable=self.entry[par]['gender'])
        self.entry[par]['radioFemele'].place(x = 610, y = row, anchor = 'nw') 

    def clearGraphic(self): # удаление лишней графики для того чтобы они не прибовлялись с каждым разом
        for par in self.mask:
            if re.search(r'name|имя', par):
                for entr in self.entry[par]:
                    if entr == 'gender':
                        continue
                    self.entry[par][entr].destroy()
                for label in self.label[par]:
                    self.label[par][label].destroy()
                continue
            if re.search(r'date|дата', par):
                continue
            self.entry[par].destroy()
            self.label[par].destroy()
        self.entry.clear() # очистка словоря так как если не делать этого поялвяется ошибка
        self.label.clear()

    def declensionsFullName(self, par): # сколнироване имени на дательный падеж и подготовка к тому чтобы быть запиханым в словарь
        declensions = {'datv'}  # datv - дательный падеж
        # print(self.entry[par]['gender'].get())
        if self.entry[par]['gender'].get() == 1:
            declensions = {'datv','femn'} # femn - женский род
        sec = self.entry[par]['entrySec'].get().strip()
        name = self.entry[par]['entryName'].get().strip()
        last = self.entry[par]['entryLast'].get().strip()
        if sec == '' or name == '' or last == '':
            messagebox.showerror("Ошибка", "Должны быть заполнены все поля")
            return False
        sec = self.morph.parse(sec)[0].inflect(declensions)
        name = self.morph.parse(name)[0].inflect(declensions)
        last = self.morph.parse(last)[0].inflect(declensions)
        if sec == None or name == None or last == None :
            messagebox.showerror("Ошибка", "в полях фамилия имя отчество находится слово которе не сколняется к падежу")
            return False
        fullName = sec[0] + " " + name[0] + " " + last[0]
        fullName = self.cap(fullName) # увеличиваем первую букву кажтого слова
        return fullName

    def getAge(self, par): # вычесление лет по поставленой дате
        date = self.entry[par].get_date()
        age = int(datetime.datetime.now().strftime("%Y")) - int(date.strftime("%Y"))
        if int(datetime.datetime.now().strftime("%m")) <= int(date.strftime("%m")) and int(datetime.datetime.now().strftime("%d")) < int(date.strftime("%d")):
            age -= 1
        if age < 0:
            age = 0
        return age

    def drawCoreWindow(self): # отрисовка основного вида окна
        self.loginLabel.destroy()
        self.loginEnter.destroy()
        self.passwordLabel.destroy()
        self.passwordEnter.destroy()
        self.authBtn.destroy()
        self.regisBtn.destroy()

        self.pathLabel = Label(self.window, text="path: ")
        self.pathLabel.place(x = 1, y = 0) 

        self.pathToFile = Label(self.window, text="")
        self.pathToFile.place(x = 30, y = 0) 

        self.browseBtn = Button(self.window, text="Browse", command=self.browse)
        self.browseBtn.place(relx = 1, rely = 0, anchor = 'ne') 

        self.btn = Button(self.window, text="save", command=self.save)
        self.btn.place(relx = 0.5, rely = 1, anchor = 's')

    def drawInput(self): # отрисовка инпутов для масок
        self.mask = self.doc.readFileAndFindMask()
        row = 25
        for par in self.mask:
            if par in self.label : #проверка для того чтобы не отрисоваывет несколко раз по одним и тем же маскам
                continue
            if re.search(r'date|дата', par) :
                continue
            if re.search(r'name|имя', par):
                self.drawNameInput(par, row)
            elif re.search(r'bearthday|день_рождение', par):
                self.label[par] = Label(self.window, text=par)
                self.label[par].place(x = 1, y = row, anchor = 'nw') 

                self.entry[par] = DateEntry()
                self.entry[par].pack()
                self.entry[par].place(x = 100, y = row, anchor = 'nw')
            else:  
                self.label[par] = Label(self.window, text=par)
                self.label[par].place(x = 1, y = row, anchor = 'nw') 
                self.entry[par] = Entry(self.window, width=30,)
                self.entry[par].place(x = 100, y = row, anchor = 'nw') 
            row += 25

    def writeContext(self): # подготовка словоря для сопостолвение для ворд документа
        context = {}
        for par in self.mask:
            if re.sub(r"[\{|\s|\}]", "", par) in context :#проверка для того чтобы не вписывать несколько раз одну и туже маску
                continue
            if re.search(r'name|имя', par):
                name = self.declensionsFullName(par)
                if name == False:
                    return False
                context[re.sub(r"[\{|\s|\}]", "", par)] = name
                continue
            if re.search(r'date|дата', par):
                context[re.sub(r"[\{|\s|\}]", "", par)] = datetime.datetime.now().strftime("%d.%m.%Y")
                continue
            if re.search(r'bearthday|день_рождение', par):
                context[re.sub(r"[\{|\s|\}]", "", par)] = str(self.getAge(par)) + ' годов'
                continue
            if self.entry[par].get().strip() == '':
                messagebox.showerror("Ошибка", "Должны быть заполнены все поля")
                return False
            context[re.sub(r"[\{|\s|\}]", "", par)] = self.entry[par].get()
        return context
    def cap(self, st):
        return ' '.join(x.capitalize() for x in st.split())